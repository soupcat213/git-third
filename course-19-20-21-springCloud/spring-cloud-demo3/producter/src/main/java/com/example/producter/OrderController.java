package com.example.producter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@RestController
public class OrderController {

    Logger logger = LoggerFactory.getLogger(OrderController.class);

    @GetMapping("/order/{id}")
    public String findById(@PathVariable("id") Long id) {
        logger.info("---------------> order");
        return "this is order-" + id;
    }

}
