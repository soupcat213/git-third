package com.example.config;

import org.redisson.api.RedissonClient;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@EnableCaching
@Configuration
public class RedisConfig {

    @Bean
    CacheManager cacheManager(RedissonClient redissonClient) {

        Map<String, CacheConfig> config = new HashMap<>();
        config.put("cache_post", new CacheConfig(20 * 1000, 8 * 1000));


        return new RedissonSpringCacheManager(redissonClient, config);
    }
}
