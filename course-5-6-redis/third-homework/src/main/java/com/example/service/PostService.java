package com.example.service;

import com.example.entity.Post;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：java思维导图
 * @since 2019-04-18
 */
public interface PostService extends IService<Post> {

    void initIndexWeekRank();


    Post get(long id);

    boolean update(Post post);

    boolean delete(long id);

}
