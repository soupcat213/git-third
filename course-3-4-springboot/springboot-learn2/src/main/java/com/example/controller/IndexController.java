package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Controller
public class IndexController extends BaseController {

    @RequestMapping({"/", "", "/index"})
    public String index(HttpServletRequest request) {

        request.setAttribute("name", "吕一明");
        return "index";
    }

}
