package com.example.service;

import com.example.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lv-success
 * @since 2019-04-10
 */
public interface UserService extends IService<User> {

}
