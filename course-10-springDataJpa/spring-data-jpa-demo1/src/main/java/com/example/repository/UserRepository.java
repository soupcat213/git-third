package com.example.repository;

import com.example.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 构建成 --> JpaRepositoryFactoryBean ( 名称是userRepository)
 *
 * @author 吕一明
 * @公众号 码客在线
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    List<User> findByOrderByIdDesc(); //--> query

    User findByNameStartingWithAndAgeLessThan(String name, int i);

    @Query("select s.name from User s where s.id = ?1")
    String findNameById(Long id);

    @Modifying
    @Transactional
    @Query("delete from User where name = ?1 and age = ?2")
    void deleteByNameAndAge(String name, int age);

    Page<User> findByNameOrderByIdDesc(Pageable pageable, String name);

}
