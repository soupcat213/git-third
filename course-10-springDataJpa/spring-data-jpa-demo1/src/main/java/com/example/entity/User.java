package com.example.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int sex;
    private int age;

    @Column(name = "clazz_id")
    private Long clazzId;


}
