package com.example.shiro;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Data
public class JwtToken implements AuthenticationToken {

    private String token;
    private String zhengshu;

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return zhengshu;
    }
}
